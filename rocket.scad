/*
 * Tiny Rocket 2021.03.13
 * Lukas Kasticky <lukas@kasticky.me>
 */

$fn = 21;

// Rocket
translate([4, -2, 0])
rocket();
module rocket()
{
    difference()
    {
        // Body
        color("#dd1111")
        rocket_body();

        // Cut-ins
        color("#cccccc")
        for (a = [0 : 360/3 : 359])
        {
            r = 2.75;
            dx = r * cos(a);
            dy = r * sin(a);    
            translate([dx,dy,0])
            rotate(a)
            linear_extrude(7)
            shape_l(.5);
        }
    }
}

// Wings

color("#eee")
translate([-6, 2, 0])
scale(.95)
wing();

color("#eee")
translate([-2.5, -6.5, 0])
rotate(90)
scale(.95)
wing();

color("#eee")
translate([2, 6, 0])
rotate(-90)
scale(.95)
wing();

// color("#ffffff")
// scale(.95)
// rotate([90, 0, 180])
// translate([-4.9, .2, -.25])
// wing();
module wing()
{
    union()
    {
        translate([0, .25, .5])
        rotate([0, 180, 0])
        linear_extrude(.5)
        union()
        {
            // Fill
            {
                points = [
                    [ 0  ,  7  ],
                    [ 1  ,  6  ],
                    [ 2  ,  4.5],
                    [ 3  ,  2  ],
                    [ 3  ,  0  ],
                    [ 3.5, -1  ],
                    [ 3.1, -3  ],
                    [ 3  , -4  ],
                    [ 0  ,  0  ],
                ];
                polygon(points);
            }

            // Sexy curves
            difference()
            {
                points = [
                    [-1  ,  7],
                    [ 5.5,  6],
                    [ 3  , -5],
                    [ 3  , -5],
                    [ 3  , -5],
                    [ 3  , -5],
                    [ 3  , -5],
                    [ 2.5, -2],
                    [ 0  ,  0],
                ];

                // rotate([90, 0, 0])
                // polygon(points);
                polyline(bezier(points, 100), 1, .25);

                // Clean cut-off
                translate([-2, -20, 0])
                square([2, 40]);
            }
        }

        // L
        translate([2, -.05, .25])
        rotate([90, 0, 180])
        linear_extrude(7.3)
        shape_l(.5);
    }
}

/*
 * Building blocks
 */

// T shape
module shape_t(scale=1)
{
    translate([scale, -scale/2, 0])
    square([3*scale, scale]);
    translate([0, -scale*1.5, 0])
    square([scale, 3*scale]);
}

// L shape
module shape_l(scale=1)
{
    translate([scale, -scale/2, 0])
    square([3*scale, scale]);
    translate([0, -scale/2, 0])
    square([scale, 2*scale]);
}

// Body
module rocket_body()
{
    points = [
        [-1, 17],
        [ 5, 19],
        [ 4,  0],
        [ 4,  0],
        [ 5,  0],
        [ 0,  0],
    ];

    // rotate(-30)
    // rotate_extrude(angle=60)
    rotate_extrude()
    union()
    {
        // Thicker bottom for cut-ins
        translate([2, 1, 0])
        square([2, 7]);

        // Base shape
        difference()
        {
            // Curve
            translate([0, .5, 0])
            polyline(bezier(points, 100), 1, .5);
            
            // Clean cut-off
            translate([-2, -20, 0])
            square([2, 40]);
        }
    }
}

/*
 * Helper functions
 * Borrowed from https://climberg.de/post/openscad_bezier_curves_of_any_degrees
 */

module line(p1, p2, w) {
    hull() {
        translate(p1) circle(r=w);
        translate(p2) circle(r=w);
    }
}

module polyline(points, index, w) {
    if(index < len(points)) {
        line(points[index - 1], points[index],w);
        polyline(points, index + 1, w);
    }
}

function choose(n, k)=
     k == 0? 1
    : (n * choose(n - 1, k - 1)) / k;

function _point_on_bezier_rec(points,t,i,c)=
    len(points) == i ? c
    : _point_on_bezier_rec(points,t,i+1,c+choose(len(points)-1,i) * pow(t,i) * pow(1-t,len(points)-i-1) * points[i]);

function _point_on_bezier(points,t)=
    _point_on_bezier_rec(points,t,0,[0,0]);

//a bezier curve with any number of control points
//parameters: 
//points - the control points of the bezier curve (number of points is variable)
//resolution - the sampling resolution of the bezier curve (number of returned points)
//returns:
//resolution number of samples on the bezier curve
function bezier(points,$fn=100)=[
for (t =[0:1.0/$fn:1+1.0/($fn/2)]) _point_on_bezier(points,t)
];